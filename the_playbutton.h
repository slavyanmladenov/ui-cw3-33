#ifndef THE_PLAYBUTTON_H
#define THE_PLAYBUTTON_H

#include <QPushButton>
#include "the_player.h"

//Derive a new class from QPushButton
//Responsible for sending play commands

class ThePlayButton : public QPushButton
{
    Q_OBJECT

    QString mCommand;   //Add a variable, save the command

public:
    explicit ThePlayButton(QWidget *parent = nullptr);

    //initialization
    //player player
    //command command string
    void init(ThePlayer* player, QString command);

private slots:
    void clicked();

signals:
    void control(QString command);
};

#endif // THE_PLAYBUTTON_H
