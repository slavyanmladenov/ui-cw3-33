//
// Created by twak on 11/11/2019.
//

#include "the_player.h"
#include <QMessageBox>

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}
void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (rand() % infos->size() );
    int n = updateCount++ % buttons->size();
    buttons -> at( n ) -> init( i );
    buttons -> at( n ) ->repaint();
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState: //This is the event when the media file is finished playing
            //Cancel auto play here
            break;
        case QMediaPlayer::State::PlayingState:
            break;
    default:
        break;
    }
}

void ThePlayer::setProgressWidget(QSlider* progressSlider)
{
    //Set the progress bar and link the signal of the progress bar drag
    mProgressSlider = progressSlider;
    if(mProgressSlider){
        connect( mProgressSlider, SIGNAL (sliderMoved(int)), SLOT ( progressSliderMoved(int) ) );
    }
}

void ThePlayer::setTimeWidget(QLabel* label)
{
    //Time display
    mTimeLabel = label;
}

void ThePlayer::setVolumeWidget(QSlider* volumeSlider)
{
    //Volume control
    mVolumeSlider = volumeSlider;
    if(mVolumeSlider){
        mVolumeSlider->setMaximumWidth(200);
        mVolumeSlider->setRange(0, 100);
        mVolumeSlider->setValue(this->volume());
        connect( mVolumeSlider, SIGNAL (sliderMoved(int)), SLOT ( volumeSliderMoved(int) ) );
    }
}

//Media file change, record maximum length (ms)
void ThePlayer::playDurationChanged(qint64 duration)
{
    mLength = duration;
}

//Play progress changes
void ThePlayer::playPositionChanged(qint64 position)
{
    //Update progress bar position
    if(mProgressSlider){
        mProgressSlider->setMaximum(mLength);
        mProgressSlider->setValue(position);
    }

    //Update time label text
    if(mTimeLabel){
        int second = position / 1000;
        int hour = second / 60 / 60;
        int minute = (second / 60) % 60;
        second = second % 60;

        char buf[32] = {0};
        snprintf(buf, 32, "%02d:%02d:%02d", hour, minute, second);
        mTimeLabel->setText(buf);
    }
}

//Progress bar drag
void ThePlayer::progressSliderMoved(int position)
{
    this->setPosition(position);
}

//Volume bar drag
void ThePlayer::volumeSliderMoved(int value)
{
    this->setVolume(value);
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}

//Execute the command sent
void ThePlayer::control(QString command){
    command = command.toLower();
    if(command == "play"){//Play
        this->play();
    }
    else if(command == "stop"){//Stop
        this->stop();
    }
    else if(command == "pause"){//Pause, click play to continue
        this->pause();
    }
    else if(command == "upload"){
        //This is to upload the currently playing video, just show a prompt dialog
        QMediaContent m = this->media();
        QString s = "upload : " + m.request().url().path();
        QMessageBox::information(
            NULL,
            QString("Upload"),
            s,
            QMessageBox::Ok);
    }
}
