//
// Created by twak on 11/11/2019.
//

#include "the_button.h"


void TheButton::init(TheButtonInfo* i) {
    setIcon( *(i->icon) );
    info =  i;
    //Change the title of the button
    //Exif data from the videos could be used 
    //but as the demo videos do not contain GPS data
    //for this demo it is hardcoded
    QString s;
    s = i->url->path();
    s += "\nLocation：America";
    this->setText(s);
}


void TheButton::clicked() {
    emit jumpTo(info);
}
