#include "the_playbutton.h"

ThePlayButton::ThePlayButton(QWidget *parent) : QPushButton(parent)
{
    //Connect click signal
    connect(this, SIGNAL(released()), this, SLOT (clicked() ));
    //Set min height
    setMinimumHeight(24);
}

void ThePlayButton::init(ThePlayer* player, QString command)
{
    //Link to the control signal of the player
    connect(this, SIGNAL(control(QString)), player, SLOT (control(QString)));
    //Set title
    this->setText(command);
    //Record command
    mCommand = command;
}

void ThePlayButton::clicked() {
    //Send command
    emit control(mCommand);
}
