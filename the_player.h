//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>

#include <QSlider>
#include <QLabel>

using namespace std;

enum PlayMode
{
    PlayModePlay,
    PlayModeStop,
    PlayModePause,
};

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;
    QTimer* mTimer;
    long updateCount = 0;
    qint64 mLength;             //When the media file changes, record the size of the media file (milliseconds)
    QSlider* mProgressSlider;   //Progress bar
    QSlider* mVolumeSlider;     //Volume controll
    QLabel* mTimeLabel;         //Time progress label

public:
      ThePlayer() : QMediaPlayer(NULL),
        mProgressSlider(NULL),  //Initialize
        mVolumeSlider(NULL),
        mTimeLabel(NULL){

        setVolume(0); // be slightly less annoying
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );

        //playing events
        //Link both signals during playback
        //Media file changes, used to get the media file size (milliseconds)
        connect( this, SIGNAL (durationChanged(qint64)), SLOT ( playDurationChanged(qint64) ) );
        //Playback progress
        connect( this, SIGNAL (positionChanged(qint64)), SLOT ( playPositionChanged(qint64) ) );
    }

    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i);
    // control
    // Set up control buttons and controls for display of playback information
    void setProgressWidget(QSlider* progressSlider);
    void setTimeWidget(QLabel* label);
    void setVolumeWidget(QSlider* volumeSlider);
private slots:

    // change the image and video for one button every one second
    void shuffle();

    void playStateChanged (QMediaPlayer::State ms);

    //Signal execution function
    void playDurationChanged(qint64 duration);
    void playPositionChanged(qint64 position);

    //Progress bar or volume drag function
    void progressSliderMoved(int position);
    void volumeSliderMoved(int value);

public slots:

    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);

    //play control
    //Function to process the sent play command
    void control(QString command);
};
#endif //CW2_THE_PLAYER_H
