/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>

#include <QLabel>
#include <QScrollArea>
#include <QScrollBar>
#include <QLineEdit>

#include "the_player.h"
#include "the_button.h"
#include "the_playbutton.h"

using namespace std;

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if true
    #if defined(_WIN32)
                if (f.contains(".wmv"))  { // windows
    #else
                if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
    #endif

#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << endl;
        }
    }

    return out;
}

//
// main
//

QVideoWidget* createPlayer();
QWidget* createMediaBox(QVideoWidget* videoWidget);
QScrollArea* createVideoList(QVideoWidget* videoWidget,
vector<TheButtonInfo>& videos, vector<TheButton*>& buttons);
QWidget* createVideoBox(QScrollArea* scrollArea);

int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // collect all the videos in the folder
    vector<TheButtonInfo> videos;


    if (argc == 2)
        videos = getInfoIn( string(argv[1]) );

    if (videos.size() == 0) {

        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );

        switch( result )
        {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }

    // a list of the buttons
    vector<TheButton*> buttons;

    QVideoWidget* videoWidget = createPlayer();
    QWidget* mediaBox = createMediaBox(videoWidget);
    QScrollArea* listWidget = createVideoList(videoWidget, videos, buttons);
    QWidget* videoBox = createVideoBox(listWidget);
    // create the main window and layout
    //Main window
    QWidget window;
    QVBoxLayout *layout = new QVBoxLayout();
    QPushButton *logo = new QPushButton();
    logo->setIcon(QIcon(QPixmap(QString(":/res/logo.png"))));
    logo->setStyleSheet("border:none; text-align:left;");
     logo->setIconSize(QSize(200, 200));
    layout->addWidget(logo);
    QHBoxLayout *top = new QHBoxLayout();   //Horizontal layout
    layout->setContentsMargins(50,50,50,50);
    top->addWidget(mediaBox);
    top->addWidget(videoBox);
    layout->addLayout(top);
    window.setLayout(layout);
    window.setWindowTitle("tomeo");
    window.setMinimumSize(800, 420);

    //Set the border distance and sub-control margin

    //Then the main window has two large container widgets, the play container and the playlist container

    // showtime!
    window.show();
    window.setStyleSheet("background-color:black; color:white;font-family: Segoe UI;font-weight:bold;");
    // wait for the app to terminate
    return app.exec();
}

//
// Player interface
//

//Create a player control
QVideoWidget* createPlayer()
{
    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    //Set the initial volume here, 100 is the maximum
    player->setVolume(50);

    return videoWidget;
}

//Control button
ThePlayButton* createControlButton(ThePlayer* player, QString command, QString icon)
{
    ThePlayButton *button = new ThePlayButton();
    button->init(player, command);
    button->setStyleSheet("background-color:none;border:none;color: transparent;width:50px;");
    button->setIcon(QIcon(QPixmap(icon)));
    button->setIconSize(QSize(50, 50));
    return button;
}

//Control box
QWidget* createControlBox(ThePlayer* player)
{
    // create the play control buttons
    //Play control box
    QWidget *ctrlWidget = new QWidget();
    QHBoxLayout *ctrlLayout = new QHBoxLayout();
    ctrlWidget->setStyleSheet("height:100px !important;");

    ctrlWidget->setLayout(ctrlLayout);

    //Several playback control buttons
    ctrlLayout->addWidget(createControlButton(player, "play", ":/res/play.png"));
    ctrlLayout->addWidget(createControlButton(player, "pause", ":/res/pause.png"));

    //Time stamp
    QLabel *timeLabel = new QLabel();
    timeLabel->setAlignment(Qt::AlignCenter);
    timeLabel->setMinimumWidth(60);
    ctrlLayout->addWidget(timeLabel);
    player->setTimeWidget(timeLabel);       //Set the time display control of the player
    //Volume label, only shows Volume
    QLabel* volumeLabel = new QLabel();
    volumeLabel->setPixmap(QPixmap(QString(":/res/volume.png")));
    volumeLabel->setStyleSheet("width:50px !important;");
    volumeLabel->setAlignment(Qt::AlignCenter); //Font alignment
    ctrlLayout->addWidget(volumeLabel);

    //Volume control bar
    QSlider *volumeSlider = new QSlider();
    volumeSlider->setOrientation(Qt::Horizontal);
    volumeSlider->setMinimumWidth(80);
    volumeSlider->setStyleSheet("QSlider::groove:horizontal { "
                                "	background-color: #FFFFFF;"
                                "	border: 1px solid #FFFFFF; "
                                "	height: 1px; "
                                "}"
                                ""
                                "QSlider::handle:horizontal { "
                                "	background-color: #FCFF6C; "
                                "	border: 2px solid #FCFF6C; "
                                "	width: 16px; "
                                "	height: 10px; "
                                "	line-height: 10px; "
                                "	margin-top: -10px; "
                                "	margin-bottom: -10px; "
                                "	border-radius: 10px; "
                                "}");
    ctrlLayout->addWidget(volumeSlider);
    player->setVolumeWidget(volumeSlider);  //Set the volume control of the player

    return ctrlWidget;
}

//Progress bar
QSlider* createPlaySlider(ThePlayer* player)
{
    //Progress
    QSlider* slider = new QSlider();
    slider->setOrientation(Qt::Horizontal);
    slider->setMinimumHeight(16);
    slider->setMaximumHeight(16);
    slider->setStyleSheet("QSlider::groove:horizontal { "
                          "	background-color: #FFFFFF;"
                          "	border: 1px solid #FFFFFF; "
                          "	height: 1px; "
                          "}"
                          ""
                          "QSlider::handle:horizontal { "
                          "	background-color: #FCFF6C; "
                          "	border: 2px solid #FCFF6C; "
                          "	width: 16px; "
                          "	height: 10px; "
                          "	line-height: 10px; "
                          "	margin-top: -10px; "
                          "	margin-bottom: -10px; "
                          "	border-radius: 10px; "
                          "}");
    player->setProgressWidget(slider);      //Set playback progress control controls

    return slider;
}

//Create a playback interface
QWidget* createMediaBox(QVideoWidget* videoWidget)
{
    ThePlayer* player = static_cast<ThePlayer*>(videoWidget->mediaObject());

    QSlider* slider = createPlaySlider(player);
    QWidget* controlBox = createControlBox(player);

    // create play layout
    // Play area container
    QWidget *MediaBox = new QWidget();
    QVBoxLayout *mediaLayout = new QVBoxLayout();
    MediaBox->setLayout(mediaLayout);

    mediaLayout->setMargin(0);
    mediaLayout->setSpacing(2);

    //Add video display controls, progress bars, and playback control boxes
    mediaLayout->addWidget(videoWidget);
    mediaLayout->addWidget(slider);
    mediaLayout->addWidget(controlBox);

    return MediaBox;
}

//
// Video list interface
//

//Create video list
QScrollArea* createVideoList(QVideoWidget* videoWidget,
    vector<TheButtonInfo>& videos, vector<TheButton*>& buttons)
{
    ThePlayer* player = static_cast<ThePlayer*>(videoWidget->mediaObject());

    // Here is the list of videos, The Button button list
    // a row of buttons
    QWidget *buttonWidget = new QWidget();

    // the buttons are arranged horizontally
    QVBoxLayout *layout = new QVBoxLayout();//Vertical layout
    buttonWidget->setLayout(layout);

    // create the four buttons
    for ( size_t i = 0; i < videos.size(); i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )),
            player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons.push_back(button);
        button->init(&videos.at(i));
        //button->setMaximumSize(200, 64);
        button->setStyleSheet("text-align: left;");
        layout->addWidget(button);
    }

    // tell the player what buttons and videos are available
    player->setContent(&buttons, &videos);

    //create QScrollArea
    //Add a scroll area
    QScrollArea *scrollArea = new QScrollArea();

    scrollArea->setWidget(buttonWidget);    //Put the video list in the scroll area
    //The horizontal scroll bar is never displayed
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //Vertical scroll bar width and scroll area style
    scrollArea->setStyleSheet(                              "QScrollBar:vertical"
                                                            " {"
                                                            "     background-color: grey;"
                                                            "border:solid 1px;"
                                                            "width:20px;"
                                                            "border-radius:10px;"
                                                            " }"
                                                            ""
                                                            " QScrollBar::handle:vertical"
                                                            " {"
                                                            "     background-color: #FCFF6C;"
                                                            "     border-radius: 10px;"
                                                            " }"
                                                            "QScrollArea{border:none;}"
                              );

    return scrollArea;
}

QWidget* createSearchBox()
{

    //Search bar
    QWidget* searchBox = new QWidget();
    QLineEdit* searchline=new QLineEdit();
    QHBoxLayout *searchboxLayout = new QHBoxLayout();
    searchBox->setLayout(searchboxLayout);  //Binding the layout of the widget, and then adding subspace in the layout, QT is this process
    searchboxLayout->setMargin(2);  //Set border width
    searchboxLayout->setSpacing(2); //Set the margins of child controls
    searchline->setPlaceholderText("  Search");
    searchline->setStyleSheet("QLineEdit{border:2px solid white; border-radius: 20px; color:white;}");

    searchboxLayout->addWidget(searchline);    //Add an edit box
    //Search button
    QPushButton* button = new QPushButton();
    button->setIcon(QIcon(QPixmap(QString(":/res/search.png"))));
    button->setToolTip("search");   //Set reminder label
    searchboxLayout->addWidget(button);

    //Delete button
    button = new QPushButton();
    button->setToolTip("delete");   //Set reminder label
    button->setIcon(QIcon(QPixmap(QString(":/res/delete.png"))));
    searchboxLayout->addWidget(button);

    //Upload button
    button = new QPushButton();
    button->setToolTip("upload");   //Set reminder label
    button->setIcon(QIcon(QPixmap(QString(":/res/upload.png"))));
    searchboxLayout->addWidget(button);

    return searchBox;
}

//Create playlist interface
QWidget* createVideoBox(QScrollArea* listWidget)
{
    QWidget* searchBox = createSearchBox();

    //play list widget
    QWidget* videosBox = new QWidget();
    QVBoxLayout *videosLayout = new QVBoxLayout();//Vertical layout

    videosBox->setLayout(videosLayout);

    //Borders and margins of child controls
    videosLayout->setMargin(0);
    videosLayout->setSpacing(2);

    //Add the search box and video list to it
    videosLayout->addWidget(searchBox);
    videosLayout->addWidget(listWidget);

    return videosBox;
}

